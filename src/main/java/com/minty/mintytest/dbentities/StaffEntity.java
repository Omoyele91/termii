/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.dbentities;

import java.io.Serializable;
import javax.persistence.*;
import lombok.*;

/**
 *
 * @author Developer
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity

@Table(name = "tbl_staffs")
public class StaffEntity implements Serializable {
     @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;   
    @Column(name = "staff_name")
    private String staffName;
    @Column(name = "phone_number")
    private String phoneNumber;
     @Column(name = "email_address")
    private String emailAddress;
    @Column(name = "address")
    private String address;
    
}
