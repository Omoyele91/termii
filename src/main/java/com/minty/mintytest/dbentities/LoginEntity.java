/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.dbentities;

import java.io.Serializable;
import javax.persistence.*;
import lombok.*;
/**
 *
 * @author Developer
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity

@Table(name = "tbl_logins")
public class LoginEntity implements Serializable {
     @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;   
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
}
