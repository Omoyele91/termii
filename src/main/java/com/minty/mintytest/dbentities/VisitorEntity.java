/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.dbentities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author Developer
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity

@Table(name = "tbl_visitors")
public class VisitorEntity implements Serializable{
     @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;   
    @Column(name = "visitor_name")
    private String visitorName;
    @Column(name = "phone_number")
    private String phoneNumber;
     @Column(name = "email_address")
    private String emailAddress;
    @Column(name = "address")
    private String address;
}
