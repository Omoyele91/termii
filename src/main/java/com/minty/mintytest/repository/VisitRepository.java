/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.repository;

import com.minty.mintytest.dbentities.VisitEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Developer
 */
public interface VisitRepository extends JpaRepository<VisitEntity, Long>{
    
}
