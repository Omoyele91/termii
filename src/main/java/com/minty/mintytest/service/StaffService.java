/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.service;

import com.minty.mintytest.dbentities.StaffEntity;
import com.minty.mintytest.models.RequestPayload;
import com.minty.mintytest.models.ResponseModel;
import com.minty.mintytest.repository.StaffRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Developer
 */
@Service
public class StaffService {
     @Autowired
    StaffRepository staffRepository;
     
      private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
      
     public ResponseModel addNewStaff(RequestPayload request)
    {
        StaffEntity staff = new StaffEntity();
        staff.setStaffName(request.getStaffName());
        staff.setPhoneNumber(request.getPhoneNumber());
        staff.setEmailAddress(request.getEmailAddress());
        staff.setAddress(request.getAddress());
        ResponseModel responseModel = new ResponseModel();
        try{
            staffRepository.save(staff);
            responseModel.setResponseCode("00");
            responseModel.setResponseMessage("Staff successfully created");
        }
        catch(Exception ex)
        {
            LOGGER.info("error occurred ::: "+ex.getMessage());
            responseModel.setResponseCode("01");
            responseModel.setResponseMessage("Error adding new staff");
        }
        return responseModel;
        
    }
     
      public ResponseModel getAllStaff()
    {
       List<StaffEntity> staffEntity = staffRepository.findAll();
        ResponseModel responseModel = new ResponseModel();
        responseModel.setResponseCode("00");
        responseModel.setResponseMessage("Staff(s) fetched successfully");
        responseModel.setData(staffEntity);
        return responseModel;
        
    } 
      
        public ResponseModel getAStaff(Long id)
    {
       Optional<StaffEntity> staffEntity = staffRepository.findById(id);
        ResponseModel responseModel = new ResponseModel();
        responseModel.setResponseCode("00");
        responseModel.setResponseMessage("Staff fetched successfully");
        responseModel.setData(staffEntity);
        return responseModel;
        
    } 
    
}
