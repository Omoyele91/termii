/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.service;

import com.minty.mintytest.dbentities.LoginEntity;
import com.minty.mintytest.models.RequestPayload;
import com.minty.mintytest.models.ResponseModel;
import com.minty.mintytest.repository.LoginRepository;
import static org.hibernate.tool.schema.SchemaToolingLogging.LOGGER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Developer
 */
@Service
public class LoginService {
    @Autowired
    LoginRepository loginRepository;
       public ResponseModel userLogin(RequestPayload request)
    {
        LoginEntity login = new LoginEntity();
        login.setUsername(request.getUsername());
        login.setPassword(request.getPassword());
        ResponseModel responseModel = new ResponseModel();
        try{
            loginRepository.save(login);
            responseModel.setResponseCode("00");
            responseModel.setResponseMessage("Login Successful");
        }
        catch(Exception ex)
        {
            LOGGER.info("error occurred ::: "+ex.getMessage());
            responseModel.setResponseCode("01");
            responseModel.setResponseMessage("Error! login failes");
        }
        return responseModel;
        
    }
}
