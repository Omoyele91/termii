/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.service;

import com.minty.mintytest.dbentities.VisitEntity;
import com.minty.mintytest.dbentities.VisitorEntity;
import com.minty.mintytest.models.RequestPayload;
import com.minty.mintytest.models.ResponseModel;
import com.minty.mintytest.repository.VisitRepository;
import com.minty.mintytest.repository.VisitorRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Developer
 */
@Service
public class VisitorService {
    @Autowired
    VisitorRepository visitorRepository;
    
     @Autowired
    VisitRepository visitRepository;
    
      private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
      
     public ResponseModel addNewVisitor(RequestPayload request)
    {
        VisitorEntity visitor = new VisitorEntity();
        visitor.setVisitorName(request.getVisitorName());
        visitor.setPhoneNumber(request.getPhoneNumber());
        visitor.setEmailAddress(request.getEmailAddress());
        visitor.setAddress(request.getAddress());
        ResponseModel responseModel = new ResponseModel();
        try{
            visitorRepository.save(visitor);
            responseModel.setResponseCode("00");
            responseModel.setResponseMessage("Visitor successfully created");
        }
        catch(Exception ex)
        {
            LOGGER.info("error occurred ::: "+ex.getMessage());
            responseModel.setResponseCode("01");
            responseModel.setResponseMessage("Error adding new visitor");
        }
        return responseModel;
        
    }
     
      public ResponseModel getAllVisitor()
    {
       List<VisitorEntity> staffEntity = visitorRepository.findAll();
        ResponseModel responseModel = new ResponseModel();
        responseModel.setResponseCode("00");
        responseModel.setResponseMessage("Visitor(s) fetched successfully");
        responseModel.setData(staffEntity);
        return responseModel;
        
    } 
      
        public ResponseModel getAVisitor(Long id)
    {
       Optional<VisitorEntity> staffEntity = visitorRepository.findById(id);
        ResponseModel responseModel = new ResponseModel();
        responseModel.setResponseCode("00");
        responseModel.setResponseMessage("Visitor fetched successfully");
        responseModel.setData(staffEntity);
        return responseModel;
        
    } 
     
      public ResponseModel addNewVisit(RequestPayload request)
    {
        VisitEntity visit = new VisitEntity();
        visit.setVisitorId(request.getVisitorId());
        visit.setStaffId(request.getStaffId());
        visit.setReasonForVisit(request.getReasonForVisit());
        visit.setDateofVisit(request.getDateofVisit());
        ResponseModel responseModel = new ResponseModel();
        try{
            visitRepository.save(visit);
            responseModel.setResponseCode("00");
            responseModel.setResponseMessage("Visit successfully created");
        }
        catch(Exception ex)
        {
            LOGGER.info("error occurred ::: "+ex.getMessage());
            responseModel.setResponseCode("01");
            responseModel.setResponseMessage("Error creating visit");
        }
        return responseModel;
        
    }
}
