/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.controller;

import com.minty.mintytest.models.RequestPayload;
import com.minty.mintytest.models.ResponseModel;
import com.minty.mintytest.service.StaffService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Developer
 */
@RestController
@RequestMapping("/staffapi")
public class StaffController {
    @Autowired
    StaffService staffService;
    
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    
      @PostMapping(value = "/staff")   
    public  ResponseModel addNewStaff(@RequestBody RequestPayload request) {
         LOGGER.info("I got to the controller::: ");
        return staffService.addNewStaff(request);
    }
    
    @GetMapping(value = "/staff")     
    public  ResponseModel getAllStaff() {        
        return staffService.getAllStaff();
    }
    
    @GetMapping(value = "/staff/{id}")   
    public  ResponseModel getAStaff(@PathVariable Long id) {
        
        return staffService.getAStaff(id);
    }
}
