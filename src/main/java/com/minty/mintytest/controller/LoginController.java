/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.controller;

import com.minty.mintytest.models.RequestPayload;
import com.minty.mintytest.models.ResponseModel;
import com.minty.mintytest.service.LoginService;
import static org.hibernate.tool.schema.SchemaToolingLogging.LOGGER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Developer
 */
@RestController
@RequestMapping("/userlogin")
public class LoginController {
    @Autowired
    LoginService loginService;
    
      @PostMapping(value = "/login")   
    public  ResponseModel userLogin(@RequestBody RequestPayload request) {
         LOGGER.info("I got to the controller::: ");
        return loginService.userLogin(request);
    }
}
