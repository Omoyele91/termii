/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.controller;

import com.minty.mintytest.models.RequestPayload;
import com.minty.mintytest.models.ResponseModel;
import com.minty.mintytest.service.VisitorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Developer
 */
@RestController
@RequestMapping("/visitorapi")
public class VisitorController {
    @Autowired
    VisitorService visitorService;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    
      @PostMapping(value = "/visitor")   
    public  ResponseModel addNewVisitor(@RequestBody RequestPayload request) {
         LOGGER.info("I got to the controller::: ");
        return visitorService.addNewVisitor(request);
    }
    
    @GetMapping(value = "/visitors")     
    public  ResponseModel getAllVissitors() {        
        return visitorService.getAllVisitor();
    }
    
    @GetMapping(value = "/visitor/{id}")   
    public  ResponseModel getAVistor(@PathVariable Long id) {
        
        return visitorService.getAVisitor(id);
    }
    
      @PostMapping(value = "/visit")   
    public  ResponseModel addNewVisit(@RequestBody RequestPayload request) {
         LOGGER.info("I got to the controller::: ");
        return visitorService.addNewVisit(request);
    }
}
