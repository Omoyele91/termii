/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minty.mintytest.models;

/**
 *
 * @author Developer
 */
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "staffName",
        "phoneNumber",
        "emailAddress",
        "address",
        "visitorName",
        "visitorId",
        "staffId",
        "reasonForVisit",
        "dateofVisit",
        "username",
        "password"
})
public class RequestPayload {
    @JsonProperty("staffName")
    private String staffName;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("emailAddress")
    private String emailAddress;
    @JsonProperty("address")
    private String address;
     @JsonProperty("visitorName")
    private String visitorName;
     @JsonProperty("visitorId")
    private String visitorId;
    @JsonProperty("staffId")
    private String staffId;
     @JsonProperty("reasonForVisit")
    private String reasonForVisit;
      @JsonProperty("dateofVisit")
    private String dateofVisit;
      @JsonProperty("username")
    private String username;
      @JsonProperty("password")
    private String password;
      
   
}
